import {
  GET_MEMBERS_BEGIN,
  GET_MEMBERS_SUCCESS,
  GET_MEMBERS_FAIL,
  POST_MEMBERS_BEGIN,
  POST_MEMBERS_FAIL,
} from "../actions/types";

const initialState = {
  members: [],
  loading: false,
  error: null,
  post: {
    loading: false,
    error: null,
  },
};

const reducerMembers = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_MEMBERS_BEGIN:
      return {
        ...state,
        loading: true,
      };
    case GET_MEMBERS_SUCCESS:
      return {
        ...state,
        members: payload,
        loading: false,
      };
    case GET_MEMBERS_FAIL:
      return {
        members: [],
        loading: false,
        error: error,
      };
    case POST_MEMBERS_BEGIN:
      return {
        ...state,
        post: {
          loading: true,
        },
      };
    case POST_MEMBERS_FAIL:
      return {
        ...state,
        post: {
          loading: false,
          error: error,
        },
      };
  }
};
export default reducerMembers;
