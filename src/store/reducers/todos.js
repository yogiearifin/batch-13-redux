import {
  GET_TODO_BEGIN,
  GET_TODO_SUCCESS,
  GET_TODO_FAIL,
  CHANGE_TODO_BEGIN,
  CHANGE_TODO_SUCCESS,
  CHANGE_TODO_FAIL,
} from "../actions/types";

const initialState = {
  todos: [],
  loading: false,
  error: null,
};

const reducerTodos = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_TODO_BEGIN:
      return {
        ...state,
        loading: true,
      };
    case GET_TODO_SUCCESS:
      return {
        todos: payload,
        loading: false,
        error: null,
      };
    case GET_TODO_FAIL:
      return {
        todos: [],
        loading: false,
        error: error,
      };
    case CHANGE_TODO_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case CHANGE_TODO_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case CHANGE_TODO_FAIL:
      return {
        todos: [],
        loading: false,
        error: error,
      };
  }
};

export default reducerTodos;
