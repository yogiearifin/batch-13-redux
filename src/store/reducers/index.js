import { combineReducers } from "redux";
import reducerMembers from "./members";
import reducerTodos from "./todos";

const rootReducers = combineReducers({
  // insert reducers here -> listing all available reducers
  reducerMembers,
  reducerTodos,
});

export default rootReducers;
