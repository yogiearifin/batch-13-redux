import {
  GET_MEMBERS_BEGIN,
  GET_MEMBERS_SUCCESS,
  GET_MEMBERS_FAIL,
  POST_MEMBERS_BEGIN,
  POST_MEMBERS_SUCCESS,
  POST_MEMBERS_FAIL,
} from "./types";
import axios from "axios";

export const getMembers = () => async (dispatch) => {
  dispatch({
    type: GET_MEMBERS_BEGIN,
    loading: true,
    error: null,
  });
  try {
    const res = await axios.get("http://localhost:4000/team-member");
    dispatch({
      type: GET_MEMBERS_SUCCESS,
      loading: false,
      payload: res.data,
      error: null,
    });
  } catch (err) {
    dispatch({
      type: GET_MEMBERS_FAIL,
      error: err.response,
    });
    console.log(err);
  }
};

export const postMembers = (body) => async (dispatch) => {
  dispatch({
    type: POST_MEMBERS_BEGIN,
    loading: true,
    error: null,
  });
  try {
    const res = await axios.post("http://localhost:4000/team-member", body);
    console.log(res);
    dispatch({
      type: POST_MEMBERS_SUCCESS,
      loading: false,
      error: null,
    });
  } catch (err) {
    dispatch({
      type: POST_MEMBERS_FAIL,
      error: err.response,
    });
  }
};
