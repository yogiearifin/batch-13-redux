import axios from "axios";
import {
  GET_TODO_BEGIN,
  GET_TODO_SUCCESS,
  GET_TODO_FAIL,
  CHANGE_TODO_BEGIN,
  CHANGE_TODO_SUCCESS,
  CHANGE_TODO_FAIL,
  ADD_TODO_BEGIN,
  ADD_TODO_SUCCESS,
  ADD_TODO_FAIL,
} from "./types";

const BASE_URL = "http://localhost:4000/todos";

export const getTodos = () => async (dispatch) => {
  dispatch({
    type: GET_TODO_BEGIN,
    loading: true,
    error: null,
  });
  try {
    const res = await axios.get(BASE_URL);
    dispatch({
      type: GET_TODO_SUCCESS,
      loading: false,
      payload: res.data,
      error: null,
    });
  } catch (err) {
    dispatch({
      type: GET_TODO_FAIL,
      error: err.response,
    });
  }
};

export const changeTodos = (id, body) => async (dispatch) => {
  dispatch({
    type: CHANGE_TODO_BEGIN,
    loading: true,
    error: null,
  });
  try {
    const res = await axios.put(`${BASE_URL}/${id}`, body);
    console.log(res);
    dispatch({
      type: CHANGE_TODO_SUCCESS,
      loading: false,
      error: null,
    });
  } catch (err) {
    dispatch({
      type: CHANGE_TODO_FAIL,
      error: err.response,
    });
  }
};

export const addTodo = (title) => async (dispatch) => {
  dispatch({
    type: ADD_TODO_BEGIN,
    loading: true,
    error: null,
  });
  try {
    const res = await axios.post(BASE_URL, title);
    dispatch({
      type: ADD_TODO_SUCCESS,
      loading: false,
      error: null,
    });
  } catch (err) {
    dispatch({
      type: ADD_TODO_FAIL,
      error: err.response,
    });
  }
};
