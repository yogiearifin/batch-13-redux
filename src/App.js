import "./App.css";
import { Provider } from "react-redux";
import store from "./store";
import MembersList from "./components/members";
import Todos from "./components/todos";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <Provider store={store}>
      {/* <MembersList /> */}
      <Todos />
    </Provider>
  );
}

export default App;
