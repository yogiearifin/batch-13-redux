import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMembers, postMembers } from "../store/actions/members";
import { Spinner } from "react-bootstrap";

const MembersList = () => {
  const dispatch = useDispatch();
  const reducerMembers = useSelector((state) => state.reducerMembers.members);
  const reducerMembersLoading = useSelector(
    (state) => state.reducerMembers.loading
  );
  const postLoading = useSelector((state) => state.reducerMembers.post.loading);
  console.log("post loading", postLoading);
  console.log("selector members", reducerMembers);
  useEffect(() => {
    dispatch(getMembers());
  }, []);

  useEffect(() => {
    if (postLoading) {
      dispatch(getMembers());
    }
  }, [postLoading]);

  const [member, setMember] = useState({
    id: Math.floor(Math.random() * 100000),
    name: "",
    stack: "",
  });
  console.log("member", member);

  const inputMember = (e) => {
    setMember({
      ...member,
      [e.target.name]: e.target.value,
    });
  };

  const submitMember = () => {
    if (member.name && member.stack) {
      dispatch(postMembers(member));
      dispatch(getMembers());
      setMember({
        id: Math.floor(Math.random() * 100000),
        name: "",
        stack: "",
      });
    } else {
      alert("please fill in the forms");
    }
  };

  return (
    <div>
      <h1>Member List</h1>
      <ul>
        {!reducerMembersLoading ? (
          reducerMembers.map((item, index) => {
            return (
              <div key={index}>
                <li>
                  {item.name} - {item.stack}
                </li>
              </div>
            );
          })
        ) : (
          <Spinner animation="border" />
        )}
      </ul>
      <div>
        <input
          type="text"
          placeholder="name"
          name="name"
          value={member.name}
          onChange={(e) => inputMember(e)}
        />
        <input
          type="text"
          placeholder="stack"
          name="stack"
          value={member.stack}
          onChange={(e) => inputMember(e)}
        />
        <button onClick={submitMember}>Submit</button>
      </div>
    </div>
  );
};
export default MembersList;
