import { useDispatch, useSelector } from "react-redux";
import { Spinner } from "react-bootstrap";
import { useEffect } from "react";
import { getTodos, changeTodos, addTodo } from "../store/actions/todos";
import "./todos.css";
import { useState } from "react";

const Todos = () => {
  const dispatch = useDispatch();
  const { todos, loading } = useSelector((state) => state.reducerTodos);
  const [title, setTitle] = useState("");
  console.log("title", title);
  useEffect(() => {
    dispatch(getTodos());
  }, []);

  const changeTodoItem = (item) => {
    dispatch(
      changeTodos(item.id, {
        id: item.id,
        title: item.title,
        isDone: !item.isDone,
      })
    );
    dispatch(getTodos());
  };

  const changeInput = (e) => {
    setTitle(e.target.value);
  };

  const submitTodo = () => {
    if (title) {
      dispatch(addTodo({ title: title, isDone: false }));
      dispatch(getTodos());
    }
  };

  return (
    <div>
      <h1>Todos</h1>
      <ul>
        {!loading ? (
          todos.map((item, index) => {
            return (
              <div key={index}>
                <li className={item.isDone ? "done" : "not-done"}>
                  {item.title}
                  <button onClick={() => changeTodoItem(item)}>
                    {!item.isDone ? "Done" : "Undo"}
                  </button>
                </li>
              </div>
            );
          })
        ) : (
          <Spinner animation="border" />
        )}
      </ul>
      <div>
        <label>Title</label>
        <input type="text" value={title} onChange={(e) => changeInput(e)} />
        <button onClick={submitTodo} disabled={title ? false : true}>
          Submit
        </button>
      </div>
    </div>
  );
};

export default Todos;
